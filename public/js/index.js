$btn_menus = document.querySelectorAll('.btn-menu');
$nav_content = document.querySelector('.nav-content');

[].forEach.call($btn_menus, function($btn_menu) {
    $btn_menu.addEventListener('click', function (event) {
        $nav_content.classList.add('transition');
        if($nav_content.classList.contains('active'))
         $nav_content.classList.remove('active');
         else 
         $nav_content.classList.add('active');
    }); 
    
});
